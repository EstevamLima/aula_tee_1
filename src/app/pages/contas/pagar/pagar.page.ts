import { NavController } from '@ionic/angular';
import { Form, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  contasForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private nav: NavController,
  ) { }

  ngOnInit() {
  }

}
